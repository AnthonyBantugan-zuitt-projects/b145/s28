// CREATE A STANDARD SERVER USING NODE:

// 1. Identify the ingredients/components needed in order to perform the task.

// http -> will allow Node JS to establish connection ion order to transfer data using the Hyper Text Transfer Protocol.
// require() -> this will allow us to acquire the resources needed to perform the task.
let http = require("http");

// 2. Create a server using methods that we will take fromt he http resources.
// the http module contains the createServer() method that will allow us to create a standard server set up.

// 3. Identify the procedure/task that the server will perform once the connection has been established.
// identify the communication between the client and server.
http.createServer(function (request, response) {
	response.end("Server is up and running");
}).listen(3000);

// 4. Select an address/location where the connection will be established or hosted.

// 5.Create a response in the terminal to make sure if the server is running successfully.
console.log("Server is running successfully");
// careful when executing this command: npx kill-port <address>

// 6. Initialize a node package manager (NPM) in our project.

// 7.Install your first package/dependency from the NPM that will allow you to make certain tasks a lot more easier.
// adjacent skills:
// install
// unsintall
// multiple installation
// syntax 1: npm install <list of dependencies>
// syntax 2: npm i <list of dependencies>
// 1. nodemon
// 2. express

// Create a remote repository to backup the project.
// NOTE: the node_modules should not be included in the files structure that will be saved online.
// 1. its going to take a longer time to stage/push your projects online.
// 2. the node_modules will cause error upon deployment.
// USE .gitignore module => will allow us to modify the components/contents that version control will track.
